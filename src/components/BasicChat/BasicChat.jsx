import React, { Component } from 'react';
import { string, object } from 'prop-types';
import styles from './BasicChat.module.scss';

class BasicChat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
      chatContent: [],
    };

    this.receiveMessage = this.receiveMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
  }

  componentDidMount() {
    const { websocket } = this.props;
    websocket.addEventListener('message', this.receiveMessage);
  }

  sendMessage() {
    const { message } = this.state;
    const { websocket, email, user } = this.props;

    websocket.send(
      JSON.stringify({
        email,
        username: user,
        message,
      }),
    );
    console.log('message sent: ', message);
    this.setState({
      message: '',
    });
  }

  receiveMessage(rawData) {
    const { chatContent } = this.state;

    const msg = JSON.parse(rawData.data);
    console.log('message received: ', msg);
    this.setState({ chatContent: [...chatContent, msg] });
  }

  render() {
    const { chatContent, message } = this.state;

    return (
      <div className={styles.container}>
        <h1 className={styles.headline}>Basic chat</h1>
        <div className={styles.chatContent}>
          {Array.from(chatContent).map(e => (
            <div key={`${e.username}${e.message}`}>
              <span className={styles.username}>{e.username}</span>
              <p className={styles.message}>{e.message}</p>
            </div>
          ))}
        </div>
        <div className={styles.myChatArea}>
          <textarea
            className={styles.textAreaForChat}
            value={message}
            onChange={e => this.setState({ message: e.target.value })}
          />
          <button className={styles.sendButton} onClick={this.sendMessage} type="submit">Send</button>
        </div>
      </div>
    );
  }
}


BasicChat.propTypes = {
  user: string.isRequired,
  email: string.isRequired,
  websocket: object.isRequired, // eslint-disable-line
};

export default BasicChat;
