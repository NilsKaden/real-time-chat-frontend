import React, { useState } from 'react';
import { func } from 'prop-types';
import styles from './Login.module.scss';

const Login = (props) => {
  const [user, setUser] = useState('');
  const [email, setEmail] = useState('');

  const { handleSubmit } = props;

  const handleForm = (e) => {
    handleSubmit(user, email);
    e.preventDefault();
  };

  return (
    <div>
      <span>Login</span>
      <form onSubmit={handleForm}>
        <label> {/* eslint-disable-line */}
          Username:
          <input className={styles.user} onChange={e => setUser(e.target.value)} />
        </label>
        <label> {/* eslint-disable-line */}
          Email:
          <input className={styles.email} onChange={e => setEmail(e.target.value)} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};

Login.propTypes = {
  handleSubmit: func.isRequired,
};

export default Login;
