
import React, { Component } from 'react';
import BasicChat from '../BasicChat/BasicChat';
import Login from '../Login/Login';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = ({
      loggedIn: false,
      user: null,
      email: null,
      websocket: null,
    });

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setState({ websocket: new WebSocket(`ws://${window.location.hostname}:8000/ws`) });
  }

  handleSubmit(user, email) {
    this.setState({
      loggedIn: (user.length > 0 && email.length > 0),
      user,
      email,
    });
  }

  render() {
    const {
      loggedIn,
      email,
      user,
      websocket,
    } = this.state;

    return (
      <div className="App">
        {!loggedIn && <Login handleSubmit={this.handleSubmit} />}
        {loggedIn && <BasicChat email={email} user={user} websocket={websocket} />}
      </div>
    );
  }
}

export default App;
